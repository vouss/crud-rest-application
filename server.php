<?php
  $_db = NULL;
  $_code = array(
                  200 => 'OK',
                  201 => 'Create',
                  404 => 'Not found',
                  500 => 'Internal server error'
                );

  try {
    _db();

    if ($_SERVER["QUERY_STRING"] == "osoby") {
      $dat = array();
      $cursor = $db->query('select * from osoby order by nazwisko,imie,id');
      sleep(1); // "progressbar"
      while ( ($rec = $cursor->fetch(PDO::FETCH_ASSOC)) ) {
        $ii = $db->query('select count(*) as v_ile from kontakty where idosoby='.$rec['id']);
        $tt = $ii->fetch(PDO::FETCH_ASSOC);
        $rec['v_ile'] = $tt['v_ile'];
        $dat[] = $rec;
      }
      sendResponse(200,$dat);
    } else if($_SERVER["QUERY_STRING"] == "create") {
      $errors = array();
      $dat = array();

      if (empty($_POST['name']) || empty($_POST['surname']))
        $errors['name'] = 'Name is required!';

      if(!empty($errors)) {
        $dat['success'] = false;
        $dat['errors'] = $errors;
        sendResponse(500,$dat);
      } else {
        $dat['success'] = true;
        $cursor = $db->query("insert into osoby(imie,nazwisko) values ('". $_POST['name'] . "','". $_POST['surname'] . "')");
        sendResponse(200,$dat);
      }
    }
  } catch(PDOException $e) {
    $_code[500] = $e->getMessage();
    sendResponse(500, $e->getMessage());
    exit();
  }

  function sendResponse( $code, $dat = array() ) {
    global $_code;
    header('HTTP/1.0 '.$code. ' '  .$_code[$code]);
    header('Content-type: application/json;charset=utf-8');
    echo json_encode($dat);
  }

  function _db() {
    global $db;
    if($db != NULL) return $db;

    $db = new PDO("sqlite:007_baza.sql");
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $db->setAttribute(PDO::ATTR_CASE, PDO::CASE_LOWER);

  }
?>